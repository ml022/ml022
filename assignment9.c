#include <stdio.h>
int main()
{
    int x,y,*a, *b, temp;
    printf("Enter the value of x\n");
    scanf("%d", &x);
    printf("Before increment\nx = %d\n", x);
    a = &x;
    b = &a+1;
    x = *a;
    y = *b;
    y = *b+1;
    printf("After increment x=%d,y=%d\n",x,y);
    return 0;
}
   